## NGINX + DOCKER + BOOTSTRAP CSS

Простой Nginx сервер контейнеризированный в Docker и запускающий статический шаблон на Bootstrap

### Команды для создания и запуска докер-образа:

```bash
# build image
docker build -t nginx_example .

# run container
docker run -d -p 8000:80 nginx_example

# confirm container is running
docker container ls
```
